<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class crawlCH extends Controller
{
	public function RunautoUpdateDevID($num, $limit, $offset)
	{
		$public_path = public_path();
		file_put_contents($public_path . '/appNew_404.txt',PHP_EOL . '**********' . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents($public_path . '/lackInfo.txt', PHP_EOL . '**********' . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents($public_path . '/list_app_update.txt', PHP_EOL . '**********' . PHP_EOL, FILE_APPEND | LOCK_EX);
		file_put_contents($public_path . '/err_appnew.txt', PHP_EOL . '**********' . PHP_EOL, FILE_APPEND | LOCK_EX);
		// shell_exec('rm -f /home/dat/laravel/public/appNew_404.txt /home/dat/laravel/public/lackInfo.txt /home/dat/laravel/public/list_app_new.txt /home/dat/laravel/public/list_app_update.txt /home/dat/laravel/public/err_appnew.txt /home/dat/laravel/public/err_request.txt /home/dat/laravel/public/list_app_err.txt');
		while ($offset < $num) {
			$cmd = 'nohup php artisan crawlCH 2 ' . $offset . ' ' . $limit . ' >/home/dat/laravel/public/nohup_err.out 2>&1&';
			shell_exec($cmd);
			$offset = $offset + $limit;
		}
	}

	public function runCrawl($offset, $limit)
	{
		$list_app = DB::table('appStore')
			->select('appid')
			->where('app_404', '<', '10')
			->where('paid_app', '0')
			->where('status3', '0')
			// ->where('relate', '')
			->offset($offset)
			->limit($limit)
			->orderBy('installs', 'desc')
			->get();
		foreach ($list_app as $app) {
			$appid = $app->appid;
			// echo $appid . PHP_EOL;
			$this->GetAppInfo($appid, 'en', 'us');
		}
	}

	public function GetAppInfo($appid, $id_lang, $id_country)
	{
		$public_path = public_path();
		// $info = array();
		// echo $appid . PHP_EOL;die;
		try {
			$response = $this->getinfoAppMultiLang($appid, $id_country, $id_lang);
			// $info['appid'] = $appid;
			// print_r($response);die;
			// file_put_contents($public_path . '/appNew_405.txt', json_encode($response));die;
			// print_r($response['info']);die;
			if ($response['status'] == false) {
				file_put_contents($public_path . '/appNew_404.txt', $appid . PHP_EOL, FILE_APPEND | LOCK_EX);
				DB::table('apk')->where('appid', $appid)->update(['status' => 1, 'app_404' => 1]);
				DB::table('appStore')->where('appid', $appid)->update(['status3' => 1, 'app_404' => 1]);
			} else {
				try {
					if (strpos($response['result'], 'error') != FALSE) {
						file_put_contents($public_path . '/appNew_404.txt', $appid . PHP_EOL, FILE_APPEND | LOCK_EX);
						DB::table('apk')->where('appid', $appid)->update(['status' => 1, 'app_404' => 1]);
						DB::table('appStore')->where('appid', $appid)->update(['status3' => 1, 'app_404' => 1]);
					} else {
						$info_appStore = array();
						$info_apk = array();
						$info_dev = array();
						$thumbnails = array();
						$info_apk['appid'] = $appid;
						if (isset($response['info']) && !empty($response['info'])) {
							// print_r($response['info']);die;
							foreach ($response['info'] as $key => $value) {
								if (isset($value[0][0])) {
									// $info['title'] = $value[0][0];
									$info_apk['title'] = $value[0][0];
								} else {
									file_put_contents($public_path . '/lackInfo.txt', $appid . "------------no title" . PHP_EOL, FILE_APPEND | LOCK_EX);
								}
								// $desc_origin = '';
								// $percent = 100;
								// print_r($value);die;

								//****************get description****************//
								if (isset($value[10][0][1]) && !empty($value[10][0][1])) {
									// $info['description'] = $value[10][0][1];
									$info_apk['description'] = $value[10][0][1];
								} else {
									file_put_contents($public_path . '/lackInfo.txt', $appid . "------------no description" . PHP_EOL, FILE_APPEND | LOCK_EX);
								}

								//****************get info****************//
								if (isset($value[12]) && !empty($value[12])) {
									foreach ($value[12] as $key1 => $value1) {
										if ($key1 == "0") {
											foreach ($value1 as $key_img => $img) {
												$thumbnails['image'][$key_img][] = $img[2][0];
												$thumbnails['image'][$key_img][] = $img[2][1];
												$thumbnails['image'][$key_img][] = $img[3][2];
											}
										}
										if ($key1 == "1") {
											if (isset($value1[3][2]) && !empty($value1[3][2])) {
												$info_apk['cover'] = $value1[3][2];
											} else {
												file_put_contents($public_path . '/lackInfo.txt', $appid . "------------no cover" . PHP_EOL, FILE_APPEND | LOCK_EX);
											}
										}
										if ($key1 == "3") {
											if (!empty($value1[0][2])) {
												$info_apk['video'] = $value1[0][2];
											}
											if (!empty($value1[1][3][2])) {
												$thumbnails['video'][] = $value1[1][3][2];
											}
										}
										if ($key1 == "4") {
											$content_rating = $value1[0];
											if (!empty($value1[2][1])) {
												$content_rating .= ',' . str_replace(", ", ",", $value1[2][1]);
											}
											$info_apk['contentrating'] = $content_rating;
											if (empty($info_apk['contentrating'])) {
												file_put_contents($public_path . '/lackInfo.txt', $appid . "------------no contentrating" . PHP_EOL, FILE_APPEND | LOCK_EX);
											}
										}
										if ($key1 == "5") {
											$dev_id = str_replace('/store/apps/developer?id=', '', $value1[5][4][2]);
											$dev_id = str_replace('/store/apps/dev?id=', '', $dev_id);
											$count_dev = DB::table('apk_developer')->where('dev_id', $dev_id)->count();
											$info_dev['dev_id'] = $dev_id;
											$info_dev['status'] = 0;
											$info_dev['domain'] = '';
											$info_dev['subdomain'] = '';
											if (isset($value1[3][5][2])) {
												$info_dev['website'] = $value1[3][5][2];
												$dev_url = parse_url($value1[3][5][2]);
												if (!empty($dev_url['host'])) {
													$domain = str_replace('www.', '', $dev_url['host']);
													$info_dev['domain'] = $domain;
												} else {
													$domain = str_replace('www.', '', $dev_url['path']);
													$check_arr = explode('/', $domain);
													if (count($check_arr) > 0) {
														$domain = $check_arr[0];
													}
													$info_dev['domain'] = $domain;
												}
											}

											if (isset($value1[2][0]) && !empty($value1[2][0])) {
												$info_dev['email'] = $value1[2][0];
											}
											if (isset($value1[4][0])) {
												$info_dev['address'] = $value1[4][0];
											}
											// print_r($info_dev);die;
											if ($count_dev == 0) {
												DB::table("apk_developer")->insertOrIgnore($info_dev);
											} else {
												DB::table('apk_developer')->where('dev_id', $dev_id)->update($info_dev);
											}
											$info_apk['dev_id'] = urldecode($dev_id);
											if (empty($info_apk['dev_id'])) {
												file_put_contents($public_path . '/lackInfo.txt', $appid . "------------no dev_id" . PHP_EOL, FILE_APPEND | LOCK_EX);
											}
										}
										/*if ($key1 == "6") {
										$info_apk['whatnew_by_lang']['origin'] = $value1[1];
									}*/
										if ($key1 == "8") {
											$info_apk['updated'] = date('Y-m-d H:i:s', $value1[0]);
											$info_apk['uploadDate'] = intval($value1[0]);
										}
										if ($key1 == "9") {
											$info_apk['installs'] = $value1[1];
											$info_appStore['installs'] = $value1[1];
											/* $info_apk['installs_string'] = $value1[0];
										$info_apk['installs_short'] = $value1[3]; */
											if (empty($info_apk['installs'])) {
												file_put_contents($public_path . '/lackInfo.txt', $appid . "------------no installs" . PHP_EOL, FILE_APPEND | LOCK_EX);
											}
										}
										if ($key1 == "13") {
											// print_r($value1[0]);die;
											$info_apk['category'] = $value1[0][0];
											// print_r($value1[0][1][4][2]);die;
											if (!empty($value1[0][2])) {
												if (strpos($value1[0][2], 'GAME') !== false) {
													$info_apk['game'] = '1';
												} else {
													$info_apk['game'] = '0';
												}
											} else {
												file_put_contents($public_path . '/lackInfo.txt', $appid . "------------no type" . PHP_EOL, FILE_APPEND | LOCK_EX);
											}
										}
										if ($key1 == "35") {
											if (!empty($value1[0])) {
												$info_apk['offerby'] = $value1[0];
											} else {
												file_put_contents($public_path . '/lackInfo.txt', $appid . "------------no offerby" . PHP_EOL, FILE_APPEND | LOCK_EX);
											}
										}
										if ($key1 == "36") {
											$info_apk['releaseDate'] = strtotime($value1);
										}
									}
									// print_r($thumbnails);
									if (!empty($thumbnails['image'])) {
										$info_apk['thumbnails'] = json_encode($thumbnails);
									} else {
										file_put_contents($public_path . '/lackInfo.txt', $appid . "------------no image" . PHP_EOL, FILE_APPEND | LOCK_EX);
									}
								}
							}
						} else {
							file_put_contents($public_path . '/list_app_new.txt', $appid . "------------" . $id_lang . PHP_EOL, FILE_APPEND | LOCK_EX);
						}

						//****************get version, androidversion****************//
						if (isset($response['androidversion']) && !empty($response['androidversion'])) {
							// print_r($response['androidversion']);die;
							if (isset($response['androidversion'][2]) && !empty($response['androidversion'][2])) {
								$info_apk['requireandroid'] = $response['androidversion'][2];
							} else {
								$info_apk['requireandroid'] = 'Varies with device';
							}
							if (isset($response['androidversion'][1]) && !empty($response['androidversion'][1])) {
								$info_apk['version'] = $response['androidversion'][1];
							} else {
								$info_apk['version'] = 'Varies with device';
							}
						} else {
							// file_put_contents($public_path . '/lackInfo.txt', $appid . "------------" . $id_country . "------------no versionString" . PHP_EOL, FILE_APPEND | LOCK_EX);
							$info_apk['requireandroid'] = 'Varies with device';
							$info_apk['version'] = 'Varies with device';
						}

						/**************** get rating ***************/
						if (isset($response['rating'][0][6][2][1]) && !empty($response['rating'][0][6][2][1])) {
							$info_apk['rate_total'] = $response['rating'][0][6][2][1];
							$info_apk['score'] = $response['rating'][0][6][0][0];
							$info_apk['commentCount'] = $response['rating'][0][6][3][1];
						}
						// print_r($response['similar'][1][1]);die;
						if (isset($response['similar'][1][1])) {
							$url_similar = $response['similar'][1][1][0][0][3][4][2];
							// print_r($url_similar);die;
							$list_app_similar = $this->similar_multilang($url_similar, $id_lang, $id_country);
							$app_similar = array();
							// print_r($list_app_similar);die;
							if (isset($list_app_similar[0][1][0][0][0])) {
								foreach ($list_app_similar[0][1][0][0][0] as $key => $similar) {
									$app_similar[] = $similar[12][0];
								}
								// print_r($app_similar);die;
								$info_apk['relate'] = json_encode($app_similar);
							} else {
								file_put_contents($public_path . '/lackInfo.txt', $appid . "------------no relate app" . PHP_EOL, FILE_APPEND | LOCK_EX);
							}
						}
						// print_r($value);die;
						if (isset($response['price']) && !empty($response['price'][0][2][0][0][0][1][0][2])) {
							$info_apk['price'] = floatval(explode("$", $response['price'][0][2][0][0][0][1][0][2])[1]);
						}
						// print_r($info_apk);
						// echo PHP_EOL;
						// print_r($info_appStore);
						// die;
						if (isset($info_apk['thumbnails']) && isset($info_apk['cover']) && isset($info_apk['title']) && isset($info_apk['dev_id']) && isset($info_apk['offerby']) && isset($info_apk['installs']) && isset($info_apk['description']) && isset($info_apk['contentrating']) && isset($info_apk['relate']) && isset($info_apk['game'])) {
							$info_appStore['status3'] = 1;
							$info_apk['status'] = 1;
							DB::table('apk')->where('appid', $appid)->update($info_apk);
							DB::table('appStore')->where('appid', $appid)->update($info_appStore);
							file_put_contents($public_path . '/list_app_update.txt', $appid . PHP_EOL, FILE_APPEND | LOCK_EX);
						}
						// DB::table('appStore')->where('appid', $appid)->update(['app_404' => 0]);
						// file_put_contents($public_path.'/404_exist.txt', $appid . PHP_EOL , FILE_APPEND | LOCK_EX);

					}
				} catch (\Exception $e) {
					// echo $appid . "******" . $e->getLine() . "******" . $e->getMessage() . PHP_EOL;
					file_put_contents($public_path . '/err_appnew.txt', $appid . "******" . $e->getLine() . "******" . $e->getMessage() . PHP_EOL, FILE_APPEND | LOCK_EX);
					file_put_contents($public_path . '/list_app_new.txt', $appid . PHP_EOL, FILE_APPEND | LOCK_EX);
				}
			}
		} catch (\Exception $e) {
			echo $appid . "******" . $e->getLine() . "******" . $e->getMessage() . PHP_EOL;
			file_put_contents($public_path . '/err_request.txt', $appid . "******" . $e->getLine() . "******" . $e->getMessage() . PHP_EOL, FILE_APPEND | LOCK_EX);
			file_put_contents($public_path . '/list_app_err.txt', $appid . PHP_EOL, FILE_APPEND | LOCK_EX);
		}
	}

	public function getinfoAppMultiLang($appid, $id_country, $id_lang, $header = array(), $proxy = array())
	{
		$getcookie = DB::table('cookie_topchart')
			->select('cookie')
			->where('id_lang', $id_lang)
			->where('id_country', $id_country)
			->first();
		// // print_r($id_lang);
		// print_r($getcookie);die;
		$cookie = $getcookie->cookie;
		// print_r($appid);
		// print_r($id_lang);die;
		$url = 'https://play.google.com/store/apps/details?id=' . $appid . '&hl=' . $id_lang . "&gl=" . $id_country;
		$res = $this->httprequest_multilang($url, $cookie);
		// $url = 'https://play.google.com/store/apps/details?id='.$appid."&hl=".$id_lang."&gl=".$id_country;
		// $res = $this->httprequest($url, 'GET', 30, $header, $proxy);
		// $config = $this->getConfig();
		$config = array(
			'info' => 'ds:6',
			'similar' => 'ds:8',
			'price' => 'ds:4',
			'rating' => 'ds:7',
			'androidversion' => 'ds:9'
		);
		$response = array();
		// print_r($res['status']);die;
		if ($res['status'] == true) {
			if ($res['msg'] != '') {
				if (strpos($res['msg'], 'We\'re sorry, the requested URL was not found on this server.') == false) {
					foreach ($config as $key => $value) {
						$html = $res['msg'];
						$matches = array();
						$string_regex = "/>AF_initDataCallback\({key: '" . $value . "'(.*?)}\);<\/script>/s";
						$t = preg_match($string_regex, $html, $matches);
						if (count($matches) > 1) {
							$tmp_string  = $matches[1];
							$tmp_string = explode('data:[', $tmp_string);
							$data_json_raw = trim($tmp_string[1]);
							$data_json_raw = '[' . $data_json_raw;
							$data_json_raw = str_replace(', sideChannel: {}', '', $data_json_raw);
							// print_r(json_decode($data_json_raw, true));die;
							$response[$key] = json_decode($data_json_raw, true);
							$response['result'] = 'success';
						} else {
							$response['result'] = " error " . $value;
						}
					}
				} else {
					$response['result'] = ' error';
				}
				// } else {
				// 	$res = $this->httprequest($url, 'GET', 30, $header, $proxy);
				// 	if ($res['msg'] != '') {
				// 		if (strpos($res['msg'], 'We\'re sorry, the requested URL was not found on this server.') == false) {
				// 			foreach ($config as $key => $value) {
				// 				$html = $res['msg'];
				// 				$matches = array();
				// 				$string_regex = "/>AF_initDataCallback\({key: '" . $value . "'(.*?)}\);<\/script>/s";
				// 				$t = preg_match($string_regex, $html, $matches);
				// 				if (count($matches) > 1) {
				// 					$tmp_string  = $matches[1];
				// 					$tmp_string = explode('data:[', $tmp_string);
				// 					$data_json_raw = trim($tmp_string[1]);
				// 					$data_json_raw = '[' . $data_json_raw;
				// 					$data_json_raw = str_replace(', sideChannel: {}', '', $data_json_raw);
				// 					// print_r(json_decode($data_json_raw, true));die;
				// 					$response[$key] = json_decode($data_json_raw, true);
				// 					$response['result'] = 'success';
				// 				}
				// 			}
				// 		} else {
				// 			$response['result'] = ' error';
				// 		}
			} else {
				$response['result'] = ' error';
			}
			// }
		} else {
			$response['result'] = ' error';
		}
		$response['status'] = $res['status'];
		return $response;
	}

	/* public function getinfoApp($appid, $header = array(), $proxy = array())
	{
		$public_path = public_path();
		$url = 'https://play.google.com/store/apps/details?id=' . $appid . "&gl=de";
		// $url = 'https://play.google.com/store/apps/details?id=com.sec.android.app.sbrowser';
		$res = $this->httprequest($url, 'GET', 30, $header, $proxy);
		$config = $this->getConfig();
		$response = array();
		if ($res['status'] == true) {
			if ($res['msg'] != '') {
				if (strpos($res['msg'], 'We\'re sorry, the requested URL was not found on this server.') == false) {
					foreach ($config as $key => $value) {
						$html = $res['msg'];

						$matches = array();
						$string_regex = "/>AF_initDataCallback\({key: '" . $value . "'(.*?)}\);<\/script>/s";
						//$string_regex = "/>AF_initDataCallback\({key: 'ds:5'(.*?)\}}\);<\/script>/s";
						//print_r($html);die;
						preg_match($string_regex, $html, $matches);
						if (count($matches) > 1) {
							$tmp_string  = $matches[1];
							$tmp_string = explode('data:[', $tmp_string);
							$data_json_raw = trim($tmp_string[1]);
							//
							$data_json_raw = '[' . $data_json_raw;
							$data_json_raw = str_replace(', sideChannel: {}', '', $data_json_raw);
							$response[$key] = json_decode($data_json_raw, true);
						}
						// print_r($response);die;
						// file_put_contents($public_path."/testtung.txt", $response);
					}
					// print_r($response['info']);
					// print_r($response['similar']);
					// print_r($response['rating']);
					// print_r($response['info']);die;
					return $response;
				} else {
					return '404';
				}
			} else {
				$res = $this->httprequest($url, 'GET', 30, $header, $proxy);
				if ($res['msg'] != '') {
					if (strpos($res['msg'], 'We\'re sorry, the requested URL was not found on this server.') == false) {
						foreach ($config as $key => $value) {
							$html = $res['msg'];
							$matches = array();
							$string_regex = "/>AF_initDataCallback\({key: '" . $value . "'(.*?)}\);<\/script>/s";
							//$string_regex = "/>AF_initDataCallback\({key: 'ds:5'(.*?)\}}\);<\/script>/s";
							//print_r($html);die;
							preg_match($string_regex, $html, $matches);
							if (count($matches) > 1) {
								$tmp_string  = $matches[1];
								$tmp_string = explode('data:[', $tmp_string);
								$data_json_raw = trim($tmp_string[1]);
								//
								$data_json_raw = '[' . $data_json_raw;
								$data_json_raw = str_replace(', sideChannel: {}', '', $data_json_raw);
								$response[$key] = json_decode($data_json_raw, true);
							}
						}
						return $response;
					} else {
						return '404';
					}
				} else {
					return 'error';
				}
			}
		} else {
			return 'error';
		}
	}*/

	public function similar_multilang($url, $id_lang, $id_country)
	{
		//$url = 'https://play.google.com/store/apps/details?id='.$appid."&hl=en&gl=us";
		$getcookie = DB::table('cookie_topchart')
			->select('cookie')
			->where('id_lang', $id_lang)
			->where('id_country', $id_country)
			->first();
		// // print_r($id_lang);
		// print_r($getcookie);die;
		$cookie = $getcookie->cookie;
		$url = 'https://play.google.com' . $url . '&gl=' . $id_country . '&hl=' . $id_lang;
		$res = $this->httprequest_multilang($url, $cookie);
		// print_r($url);die;
		// $value = 'ds:7';
		$response = array();
		if ($res['status'] == true) {
			if ($res['msg'] != '') {
				$html = $res['msg'];
				$matches = array();
				$string_regex = "/>AF_initDataCallback\({key: \'ds:4\'(.*?)}\);<\/script>/s";
				//print_r($string_regex);
				$t = preg_match($string_regex, $html, $matches);
				// print_r($matches);die;
				if (count($matches) > 1) {
					$tmp_string  = $matches[1];
					//print_r($tmp_string);
					$tmp_string = explode('data:[', $tmp_string);
					$data_json_raw = trim($tmp_string[1]);
					$data_json_raw = str_replace(', sideChannel: {}', '', $data_json_raw);
					$response = json_decode('[' . $data_json_raw, true);
					// print_r($response);die;
					return $response;
				} else {
					return 'error';
				}
			} else {
				return 'error';
			}
		} else {
			return 'error';
		}
	}

	public function httprequest($url, $method, $timeout = 30, $header = array(), $proxy = array(), $filter = array(), $useTor = 0, $cookie = "")
	{
		$curl = curl_init();
		//print_r($filter);
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => $timeout,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $method,
			//CURLOPT_POSTFIELDS => $body,
			CURLOPT_FOLLOWLOCATION => true,

			// CURLOPT_PROXYTYPE => 7,
			// CURLOPT_PROXY => "195.201.192.254",
			// CURLOPT_PROXYPORT => "28982"
		));
		if (!empty($filter)) {
			if (isset($filter['file'])) {
				$file = $filter['file'];
				curl_setopt($curl, CURLOPT_COOKIEJAR, $file);
				curl_setopt($curl, CURLOPT_COOKIEFILE, $file);
			}
			if ($filter['statuscode'] == true) {
				curl_setopt($curl, CURLOPT_HEADER, true);  // we want headers
				curl_setopt($curl, CURLOPT_NOBODY, true);
			}
		}
		if ($method == 'POST') {
			curl_setopt($curl, CURLOPT_POSTFIELDS, $filter['body']);
		}
		if (!empty($proxy)) {
			curl_setopt($curl, CURLOPT_PROXYTYPE, 7);
			curl_setopt($curl, CURLOPT_PROXY, $proxy['host']);
			curl_setopt($curl, CURLOPT_PROXYPORT, $proxy['port']);
		}
		if (!empty($header)) {
			$params = array();
			foreach ($header as $key => $value) {
				$params[] = $key . ":" . $value;
			}
			curl_setopt($curl, CURLOPT_HTTPHEADER, $params);
		}
		// if (!empty($cookie)) {
		// curl_setopt($curl, CURLOPT_HTTPHEADER, array("Cookie:".$cookie));
		// }
		if ($useTor == 1) {
			curl_setopt($curl, CURLOPT_PROXY, 'http://localhost:9050');
			curl_setopt($curl, CURLOPT_PROXYTYPE, 7);
		}
		// if($xmlRequest !== false){
		// curl_setopt($curl, CURLOPT_HTTPHEADER, array("X-Requested-With: XMLHttpRequest","content-length: ".strlen($body),
		// "content-type: application/x-www-form-urlencoded; charset=UTF-8","referer: ".$referer,'x-csrf-token:'.$token));
		// }

		$response = curl_exec($curl);

		// $public_path = public_path();
		// file_put_contents($public_path."/aaadfffbb.txt", $response);
		$err = curl_error($curl);
		if ($err) {
			$res = array('status' => false, 'err' => $err);
			return $res;
		}
		if (!empty($filter)) {
			if (isset($filter['statuscode']) && $filter['statuscode'] == true) {
				$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
				$res = array('status' => true, 'msg' => $httpcode);
			}
		}
		return $res = array('status' => true, 'msg' => $response);
	}

	public function httprequest_multilang($url, $cookie)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"cookie: " . $cookie,
			),
		));

		$response = curl_exec($curl);
		$status_code =  curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		// print_r($response);die;
		// $public_path = public_path();
		// file_put_contents($public_path . '/appNew_404.txt', $response);die;
		if ($status_code == 200) {
			return $res = array('status' => true, 'msg' => $response);
		}
	}

	/* public function getConfig()
	{

		//ds:7 : similar
		// ds: 5 // info
		// ds: 3 price
		// ds:6, rating,review
		// ds: 8 androidversion
		// ds:9 size + androidversion min

		$config = array(
			'info' => 'ds:5',
			'similar' => 'ds:7',
			'price' => 'ds:3',
			'rating' => 'ds:6',
			'androidversion' => 'ds:8'
		);
		return $config;
	} */
}
