<?php

namespace App\Http\Controllers;

require_once(__DIR__.'/simple_html_dom.php');
use Illuminate\Http\Request;
use DB;

class CouponController extends Controller
{
	public function AddLink ($url) {
		$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $url,
			CURLOPT_USERAGENT => 'Tung test cURL Request',
			CURLOPT_SSL_VERIFYPEER => false
		));
		$resp = curl_exec($curl);
		curl_close($curl);
		$html = str_get_html($resp);
		$data_link = $html->find('div');
		$list_page = array();
		$list_link = array();
		$list_search = array();
		$data['url'] = $url;
		$data['type'] = '1';
		$data['status'] = '1';
		DB::table('couponxoo_list')->insertOrIgnore($data);
		$data = array();
		foreach ($data_link as $value) {
			$data = array();
			$type = $value->getAttribute('data-code_type');
			if ($type == 'code' or $type == 'deal') {
				$slug_name = $value->getAttribute('data-store_slug');
				$link = $url.'coupons/'.$slug_name;
				$data['url'] = $link;
				$data['type'] = '2';
				$data['status'] = '0';
				if (!in_array($link, $list_link)) {
					$list_link[] = $link;
					DB::table('couponxoo_list')->insertOrIgnore($data);
					$list_page[] = DB::getPdo()->lastInsertId();
				}
			}
		}
		$data_link2 = $html->find('div.draggable a');
		foreach ($data_link2 as $value) {
			$data = array();
			if (strpos($value, 'coupons') !== false) {
				$code = $value->href;
				$code = trim($code, '/');
				$link = $url . $code;
				$data['url'] = $link;
				$data['type'] = '2';
				$data['status'] = '0';
				if (!in_array($link, $list_link)) {
					$list_link[] = $link;
					DB::table('couponxoo_list')->insertOrIgnore($data);
					$list_page[] = DB::getPdo()->lastInsertId();
				}
			}
		}
		$data_link3 = $html->find('div.p-brands a');
		foreach ($data_link3 as $value) {
			$data = array();
			$data1 = array();
			if (strpos($value, 'coupons') !== false) {
				$code = $value->href;
				$code = trim($code, '/');
				$link = $url . $code;
				$data['url'] = $link;
				$data['type'] = '2';
				$data['status'] = '0';
				if (!in_array($link, $list_link)) {
					$list_link[] = $link;
					DB::table('couponxoo_list')->insertOrIgnore($data);
					$list_page[] = DB::getPdo()->lastInsertId();
				}
			} 
			// else {
				// $code = $value->href;
				// $code = trim($code, '/');
				// $link = $url . $code;
				// $data1['key_search'] = trim($value->plaintext, ' ');
				// $data['url_search'] = $link;
				// $data['type'] = '2';
				// $data['status'] = '0';
				// if (!in_array($link, $list_link)) {
					// $list_link[] = $link;
					// DB::table('key_search')->insertOrIgnore($data1);
					// DB::table('search_list2')->insertOrIgnore($data);
					// $list_search[] = DB::getPdo()->lastInsertId();
				// }
			// }
		}
		return '';
	}
	public function AddCoupon ($list_link) {
		// var_dump($list_link);die;
		if (empty($list_link)) {
			$list_page = DB::table('couponxoo_list')
						->select('url')
						->where('type', '2')
						->where('status', '0')
						->get();
		} else {
			$list_page = $list_link;
		}
		foreach ($list_page as $url) {
			if (empty($list_link)) {
				$url = $url->url;
			}
			$curl = curl_init();
				curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $url,
				CURLOPT_USERAGENT => 'Tung test cURL Request',
				CURLOPT_SSL_VERIFYPEER => false
			));
			$resp = curl_exec($curl);
			curl_close($curl);
			$html = str_get_html($resp);
			$find = $html->find('a.a_code');
			$find_url = $html->find('div');
			$find_title_c = $html->find('div.deal-desc a');
			$find_desc_c = $html->find('div.deal-desc p');
			$find_disc = $html->find('div.discount');
			$data = array();
			$advertiser = array();
			$coupon_description = array();
			$coupon_title = array();
			$advertiser_url = array();
			$coupon_url = array();
			$coupon_id = array();
			foreach ($find as $value1) {
				if (empty($value1->children(0)->plaintext)) {
					$coupon_id[] = trim($value1->plaintext);
				} else {
					$coupon_id[] = $value1->children(0)->plaintext;
				}
			}
			foreach ($find_title_c as $value1) {
				$coupon_title[] = $value1->plaintext;
			}
			foreach ($find_desc_c as $value1) {
				$coupon_description[] = trim($value1->plaintext, ' ');
			}
			foreach ($find_url as $value1) {
				$type = $value1->getAttribute('data-code_type');
				if ($type == 'code') {
					$code_url = $value1->id;
					$coupon_url[] = $url .'#'. $code_url;
					$advertiser[] = $value1->getAttribute('data-store_name');
					$advertiser_url[] = $value1->getAttribute('data-store_domain');
				} elseif ($type == 'deal') {
					$code_url = $value1->id;
					$coupon_url[] = $url .'#'. $code_url;
					$advertiser[] = $value1->getAttribute('data-store_name');
					$advertiser_url[] = $value1->getAttribute('data-store_domain');
				}
			}
			foreach ($find_disc as $key => $value1) {
				$request = array();
				$discount = trim($value1->plaintext, ' ');
				$count_word = strlen($discount);
				$unit = substr($discount, $count_word-1, $count_word);
				$request['coupon_id'] = $coupon_id[$key];
				if ($unit == '%') {
					$request['discount_percent'] = (int)$discount;
					$request['discount'] = null;
					$request['currency'] = null;
				} elseif($discount == 'SALE') {
					$request['discount_percent'] = null;
					$request['currency'] = null;
					$request['discount'] = null;
				} elseif (filter_var($unit, FILTER_VALIDATE_INT) === false) {
					$request['currency'] = $unit;
					$request['discount'] = (int)$discount;
					$request['discount_percent'] = null;
				} else {
					$request['discount_percent'] = null;
					$request['currency'] = substr($discount, 0, 1);
					$request['discount'] = (int)substr($discount, 1, $count_word);
				}
				$request['coupon_title'] = $coupon_title[$key];
				$request['advertiser'] = $advertiser[$key];
				$request['coupon_description'] = $coupon_description[$key];
				$request['coupon_url'] = $coupon_url[$key];
				$request['advertiser_url'] = $advertiser_url[$key];
				$request['begin_date'] = null;
				$request['expired_date'] = null;
				$request['status'] = '1';
				// $data[] = $request;
				DB::table('coupon_detail')->insertOrIgnore($request);
				// echo '<pre>'; print_r($request);
				DB::table('couponxoo_list')->where('url',$url)->update(['status' => '1']);
			}
		}
	}
	public function UpdateExpiredCoupon() {
		$list_page = DB::table('couponxoo_list')
					->select('url')
					->where('type', '2')
					->get();
		foreach ($list_page as $url) {
			$url = $url->url;
			$curl = curl_init();
				curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $url,
				CURLOPT_USERAGENT => 'Tung test cURL Request',
				CURLOPT_SSL_VERIFYPEER => false
			));
			$resp = curl_exec($curl);
			curl_close($curl);
			$html = str_get_html($resp);
			$find = $html->find('div#full-text>div');
			foreach ($find as $value1) {
				$type = $value1->getAttribute('data-code_type');
				if ($type == 'code' or $type == 'deal') {
					$code_url = $value1->id;
					$coupon_url = $url .'#'. $code_url;
					$advertiser = $value1->getAttribute('data-store_name');
					DB::table('coupon_detail')
						->where('coupon_url',$coupon_url)
						->where('advertiser',$advertiser)
						->update(['status' => '0']);
				}
			}
		}
	}
	public function NewStoreCoupon() {
		$main_url = DB::table('couponxoo_list')
			->select('url')
			->where('type', '1')
			->first();
		$main_url = $main_url->url;
		$list_page = DB::table('couponxoo_list')
			->select('url')
			->where('type', '2')
			->get();
		$list_link = array();
		foreach ($list_page as $url) {
			$url = $url->url;
			$curl = curl_init();
				curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $url,
				CURLOPT_USERAGENT => 'Tung test cURL Request',
				CURLOPT_SSL_VERIFYPEER => false
			));
			$resp = curl_exec($curl);
			curl_close($curl);
			$html = str_get_html($resp);
			$find = $html->find('div.new-store a');
			foreach ($find as $value) {
				$code = $value->href;
				$data = array();
				if (strpos($value, '/coupons/') !== false) {
					$code = trim($code, '/');
					$link = $main_url . $code;
					$data['url'] = $link;
					$data['type'] = '2';
					$data['status'] = '0';
					if (!in_array($link, $list_link)) {
						$list_link[] = $link;
						DB::table('couponxoo_list')->insertOrIgnore($data);
					}
				} else {
					$code = trim($code, '/');
					$link = $main_url . $code;
					$data['key_search'] = trim($value->plaintext, ' ');
					$data['status'] = 0;
					DB::table('key_search')->insertOrIgnore($data);
					$request = array();
					$request['url_search'] = $link;
					$request['type'] = '2';
					$request['status'] = '0';
					DB::table('search_list2')->insertOrIgnore($request);
				}
			}
		}
		return $list_link;
	}
	public function SearchDetail() {
		$main_url = DB::table('couponxoo_list')
			->select('url')
			->where('type', '1')
			->where('id', '1')
			->first();
		$main_url = $main_url->url;
		$list_page = DB::table('search_list2')
			->select('url_search')
			->where('type', '2')
			->get();
		$list_link = array();
		foreach ($list_page as $url) {
			$url = $url->url_search;
			$curl = curl_init();
				curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $url,
				CURLOPT_USERAGENT => 'Tung test cURL Request',
				CURLOPT_SSL_VERIFYPEER => false
			));
			$resp = curl_exec($curl);
			curl_close($curl);
			$html = str_get_html($resp);
			if (!empty($html)) {
				$data = array();
				$find = $html->find('div.show-link a');
				$find_keyword = $html->find('div.deal-desc h2');
				$find_moreoffer = $html->find('div.text-truncate a');
				foreach ($find as $value) {
					$data['url_goto'][] = trim($value->plaintext, ' ');
				}
				foreach ($find_moreoffer as $value) {
					$url_more_offer = $value->href;
					if (strpos($url_more_offer, '/more-offers/') !== false) {
						$url_more_offer = trim($url_more_offer, '/');
						$data['url_more_offer'][] = $main_url.$url_more_offer;
					}
				}
				foreach ($find_keyword as $key => $value) {
					$request = array();
					$request['keyword'] = trim($value->plaintext, ' ');
					$request['url_goto'] = $data['url_goto'][$key];
					$request['url_more_offer'] = $data['url_more_offer'][$key];
					DB::table('search_detail')->insertOrIgnore($request);
				}
			}
		}
	}
	public function GetMoreKeySearch($start, $end) {
		$main_url = DB::table('couponxoo_list')
			->select('url')
			->where('type', '1')
			->where('id', '1')
			->first();
		$main_url = $main_url->url;
		$list_page = DB::table('search_list2')
			->select('url_search')
			->whereBetween('id', [$start,$end])
			->where('type', '2')
			->where('status', '0')
			->get();
		// echo '<pre>'; print_r($list_page);
		$list_link = array();
		if (!empty($list_page)) {
			foreach ($list_page as $url) {
				$url = $url->url_search;
				$curl = curl_init();
					curl_setopt_array($curl, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => $url,
					CURLOPT_USERAGENT => 'Tung test cURL Request',
					CURLOPT_SSL_VERIFYPEER => false
				));
				$resp = curl_exec($curl);
				curl_close($curl);
				$html = str_get_html($resp);
				if (!empty($html)) {
					$find = $html->find('div.p-brands a');
					$find_search = $html->find('a.kw_related');
					foreach ($find as $value) {
						$data = array();
						$data['key_search'] = trim($value->plaintext, ' ');
						$data['status'] = 0;
						DB::table('key_search')->insertOrIgnore($data);
						$request = array();
						$url_search = $value->href;
						if (strpos($url_search, '/stores/') !== false) {
							$url_search = trim($url_search, '/');
							$request['url'] = $main_url.$url_search;
							$request['type'] = 2;
							DB::table('couponxoo_list')->insertOrIgnore($request);
						} else {
							$url_search = trim($url_search, '/');
							$request['url_search'] = $main_url.$url_search;
							$request['type'] = 2;
							DB::table('search_list2')->insertOrIgnore($request);
						}
					}
					foreach ($find_search as $value) {
						$data = array();
						$data['key_search'] = $value->title;
						$data['status'] = 0;
						DB::table('key_search')->insertOrIgnore($data);
						$request = array();
						$url_search = $value->title;
						$url_search = str_replace(' ', '-', $url_search);
						$request['url_search'] = $main_url.$url_search;
						$request['type'] = 2;
						DB::table('search_list2')->insertOrIgnore($request);
					}
				}
			}
		}
	}
	public function GetDomain($start, $end) {
		$main_url = DB::table('couponxoo_list')
			->select('url')
			->where('type', '1')
			->where('id', '1')
			->first();
		$main_url = $main_url->url;
		$list_page = DB::table('search_list2')
			->select('url_search')
			->whereBetween('id', [$start,$end])
			->where('type', '2')
			->where('status', '0')
			->get();
		// echo '<pre>'; print_r($list_page);
		$list_link = array();
		if (!empty($list_page)) {
			foreach ($list_page as $url) {
				$url = $url->url_search;
				$curl = curl_init();
					curl_setopt_array($curl, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => $url,
					CURLOPT_USERAGENT => 'Tung test cURL Request',
					CURLOPT_SSL_VERIFYPEER => false
				));
				$resp = curl_exec($curl);
				curl_close($curl);
				$html = str_get_html($resp);
				if (!empty($html)) {
					$find = $html->find('div.show-link a');
					foreach ($find as $value) {
						$request = array();
						$url_goto = trim($value->plaintext, ' ');
						$domain = str_ireplace('www.', '', parse_url($url_goto, PHP_URL_HOST));
						$request['domain'] = $domain;
						$request['url_goto'] = $url_goto;
						DB::table('url_out')->insertOrIgnore($request);
					}
				}
				DB::table('search_list2')
					->where('url_search',$url)
					->update(['status' => '1']);
			}
		}
	}
	public function UpdateAlexa() {
		$filename = public_path('app/top-1m.csv');
        if (($handle = fopen("app/top-1m.csv", "r")) !== FALSE) 
        {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			{
				DB::table('url_out')
					->where('domain',$data[1])
					->update(['alexa_rank' => $data[0]]);
			}
			fclose($handle);
		}
	}
	public function ReInsertUrlSearch($start, $end) {
		$list_page = DB::table('search_list')
			->select('url_search', 'type')
			->whereBetween('id', [$start,$end])
			->get();
		foreach ($list_page as $value) {
			$url_search = $value->url_search;
			$url = str_ireplace('www.', '', parse_url($url_search));
			$path = str_replace('//', '/', $url['path']);
			$domain = 'https://www.couponxoo.com'.$path;
			$request['url_search'] = $domain;
			$request['type'] = 2;
			$request['status'] = 0;
			DB::table('search_list2')->insertOrIgnore($request);
		// echo '<pre>'; print_r($domain);
		// echo PHP_EOL;
		}
		// echo '<pre>'; print_r($list_page);die;
	}
}
