<?php

namespace App\Http\Controllers;

require_once(__DIR__ . '/simple_html_dom.php');

use Illuminate\Http\Request;
use DB;

class versionCompareDistributed extends Controller
{
    private function checkDownload()
    {
        $apps = DB::table("apkNo")
            ->select("appid")
            ->where("status", "0")
            ->get();
    }

    private function get_headers_from_curl_response($response)
    {
        $headers = array();
        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));
        foreach (explode("\r\n", $header_text) as $i => $line)
            if ($i === 0) {
                $headers['http_code'] = $line;
            } else {
                list($key, $value) = explode(': ', $line);
                $headers[$key] = $value;
            }

        return $headers;
    }

    private function crawlTrending($url)
    {
        // $title = "";
        // print_r($url);
        // echo PHP_EOL;
        $curlTrending1 = curl_init();
        curl_setopt_array($curlTrending1, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_URL => $url,
            CURLOPT_CONNECTTIMEOUT => 30,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52',
            CURLOPT_HEADER => TRUE
        ));
        $htmlTrending = curl_exec($curlTrending1);
        // $resTime = curl_getinfo($curlTrending1, CURLINFO_TOTAL_TIME);
        $http_code = curl_getinfo($curlTrending1, CURLINFO_HTTP_CODE);
        curl_close($curlTrending1);
        if ($http_code == 500) {
            // $cache_status = "0";
            // $cf_ray = "0";
            // $versionApktrending = 0;
            $dateTrending = 0;
            // $title = NULL;
            // $versionCode = 0;
            // $resTime = 0;
        } else {
            $header = $this->get_headers_from_curl_response($htmlTrending);
            if ($http_code == 301) {
                $urlTrending1 = $header["Location"];
                $curlTrending2 = curl_init();
                curl_setopt_array($curlTrending2, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_URL => $urlTrending1,
                    CURLOPT_CONNECTTIMEOUT => 30,
                    CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52',
                    CURLOPT_HEADER => TRUE
                ));
                $htmlTrending1 = curl_exec($curlTrending2);
                // $resTime = curl_getinfo($curlTrending2, CURLINFO_TOTAL_TIME);
                $http_code = curl_getinfo($curlTrending2, CURLINFO_HTTP_CODE);
                curl_close($curlTrending2);
                if ($http_code == 500) {
                    // $cache_status = "0";
                    // $cf_ray = "0";
                    // $versionApktrending = 0;
                    $dateTrending = 0;
                    // $title = NULL;
                    // $versionCode = 0;
                    // $resTime = 0;
                } else {
                    // $header1 = $this->get_headers_from_curl_response($htmlTrending1);
                    // $http_code = intval(explode(" ", $header1["http_code"])[1]);
                    // var_dump($http_code);
                    // $cache_status = $header1["CF-Cache-Status"];
                    // var_dump($cache_status);
                    // $cf_ray = explode("-", $header1["CF-RAY"])[1];
                    // var_dump($cf_ray);
                    // var_dump($resTime);
                    if ($http_code >= 400) {
                        // echo "loi 404" . PHP_EOL;
                        // $versionApktrending = 0;
                        $dateTrending = 0;
                        // $title = NULL;
                        // $versionCode = 0;
                    } else {
                        $docTrending = str_get_html($htmlTrending1);
                        // $size = trim(explode(" ", $docTrending->find("table[class=table table-responsive border rounded mb-1] td.pl-4", 2)->plaintext)[0]);
                        // if (strcmp($size, "0") != 0) {
                        // var_dump($title);
                        if ($docTrending->find('h1[class=apk-detail__title apk-detail__title--safe font-weight-light]', 0)) {
                            // $title = $docTrending->find("title", 0)->innertext;
                            // $plain_text = $docTrending->find("table[class=table table-responsive border rounded mb-1] td.pl-4", 1)->plaintext;
                            // $versionInfo = explode(" (", $plain_text);
                            // $versionApktrending = trim($versionInfo[0]);
                            $dateTrendingStr = $docTrending->find("table[class=table table-responsive border rounded mb-1] td.pl-4", 3)->plaintext;
                            $dateTrending = strtotime($dateTrendingStr);
                            // print_r($versionApktrending);
                            // echo PHP_EOL;
                            // echo $dateTrending . PHP_EOL;
                            // $versionCode = intval(explode(")", $versionInfo[1])[0]);
                        } else {
                            // $versionApktrending = 0;
                            $dateTrending = 0;
                            // $versionCode = 0;
                            // $title = $docTrending->find("title", 0)->innertext;
                        }
                        // } else {
                        // }
                    }
                }
            } else {
                // $http_code = intval(explode(" ", $header["http_code"])[1]);
                // var_dump($http_code);
                // $cache_status = $header["CF-Cache-Status"];
                // var_dump($cache_status);
                // $cf_ray = explode("-", $header["CF-RAY"])[1];
                // var_dump($cf_ray);
                // var_dump($resTime);
                if ($http_code >= 400) {
                    // echo "loi 404" . PHP_EOL;
                    // $versionApktrending = 0;
                    $dateTrending = 0;
                    // $title = NULL;
                    // $versionCode = 0;
                } else {
                    $docTrending = str_get_html($htmlTrending);
                    if ($docTrending->find('h1[class=apk-detail__title apk-detail__title--safe font-weight-light]', 0)) {
                        // $title = $docTrending->find("title", 0)->innertext;
                        // $plain_text = $docTrending->find("table[class=table table-responsive border rounded mb-1] td.pl-4", 1)->plaintext;
                        // $versionInfo = explode(" (", $plain_text);
                        // $versionApktrending = trim($versionInfo[0]);
                        $dateTrendingStr = $docTrending->find("table[class=table table-responsive border rounded mb-1] td.pl-4", 3)->plaintext;
                        $dateTrending = strtotime($dateTrendingStr);
                        // print_r($versionApktrending);
                        // echo PHP_EOL;
                        // echo $dateTrending . PHP_EOL;
                        // $versionCode = intval(explode(")", $versionInfo[1])[0]);
                    } else {
                        // $versionApktrending = 0;
                        $dateTrending = 0;
                        // $versionCode = 0;
                    }
                }
            }
        }
        // $app['url'] = $url;
        // $app['version'] = $versionApktrending;
        // $app['versionCode'] = $versionCode;
        $app['date'] = $dateTrending;
        // $app['http_code'] = $http_code;
        // $app['cache_status'] = $cache_status;
        // $app['cf_ray'] = $cf_ray;
        // $app['resTime'] = $resTime;
        // $app['title'] = $title;
        return $app;
    }

    public function stats()
    {
        date_default_timezone_set('asia/ho_chi_minh');
        $slaves = DB::table('location')
            ->select("location", 'city')
            ->where("topic", '!=', '0')
            ->get();

        $city = "";
        foreach ($slaves as $slave) {
            $location = $slave->location;
            $city = $slave->city;
            $timeBegin = strtotime("1970-01-01");
            $date = strval(date("Y-m-d"));

            $result = DB::table('versioncompare')->join('url', 'versioncompare.urlID', '=', 'url.id')
                ->select('url', 'apkpure', 'apkmonk', 'apkcombo', 'apksupport', 'chplay', 'statusCode', 'cacheStatus', 'resTime', 'type')
                ->where('dateCheck', 'like', '%' . $date . '%')
                ->where('location', 'like', '%' . $location . '%')
                ->where('city', 'like', '%' . $city . '%')
                // ->where('isGA', '1')
                ->limit(100)
                ->groupBy('url', 'apkpure', 'apkmonk', 'apkcombo', 'apksupport', 'chplay', 'statusCode', 'cacheStatus', 'resTime', 'type')
                ->get();
            if (sizeof($result) != 0) {
                $sum = sizeof($result);
                $hitNum = 0;
                $statusNum = 0;
                $errorNum = 0;
                $resTime = 0;
                $onlyHave = 0;
                $dontHave = 0;
                $faster = 0;
                $gaNum = 0;
                $pureNum = 0;
                $comboNum = 0;
                $supportNum = 0;
                $pureNum1 = 0;
                $comboNum1 = 0;
                $supportNum1 = 0;
                $notVaries = 0;
                $equalCh = 0;

                foreach ($result as $record) {
                    $appTrending = $this->crawlTrending($record->url);
                    if (strcmp($record->cacheStatus, "HIT") == 0) {
                        $hitNum++;
                    }

                    if (strcmp($record->type, "GA") == 0) {
                        $gaNum++;
                    }

                    if ($record->statusCode == 200) {
                        $statusNum++;
                        if ($record->apkpure == 0 && $record->apkcombo == 0 && $record->apksupport && 0) {
                            $onlyHave++;
                        }

                        if ($record->apkpure == 2 && $record->apkcombo == 2 && $record->apksupport == 2) {
                            $faster++;
                        }

                        if (strcmp($record->chplay, "varies") != 0 && strcmp($record->chplay, "0") != 0) {
                            $notVaries++;
                            if (strcmp($record->chplay, "1") == 0) {
                                $equalCh++;
                            }
                        }

                        if ($record->apkpure == 2) {
                            $pureNum++;
                        }

                        if ($record->apkcombo == 2) {
                            $comboNum++;
                        }

                        if ($record->apksupport == 2) {
                            $supportNum++;
                        }

                        if ($record->apkpure == -1) {
                            $pureNum1++;
                        }

                        if ($record->apkcombo == -1) {
                            $comboNum1++;
                        }

                        if ($record->apksupport == -1) {
                            $supportNum1++;
                        }

                        if (($record->apkpure != 0 || $record->apkcombo != 0 || $record->apksupport != 0) && ($appTrending['date'] - $timeBegin) < 86400) {
                            $dontHave++;
                        }
                    } else {
                        $errorNum++;
                        if ($record->apkpure != 0 || $record->apkcombo != 0 || $record->apksupport != 0) {
                            $dontHave++;
                        }

                        if ($record->apkpure != 0) {
                            $pureNum1++;
                        }

                        if ($record->apkcombo != 0) {
                            $comboNum1++;
                        }

                        if ($record->apksupport != 0) {
                            $supportNum1++;
                        }
                    }
                    $resTime = $resTime + $record->resTime;
                }

                $statHit = $hitNum / $sum * 100;
                $statPass = $statusNum / $sum * 100;
                $statError = $errorNum / $sum * 100;
                // $statOnly = $onlyHave / $sum * 100;
                // $statDont = $dontHave / $sum * 100;
                $statFaster = $faster / $sum * 100;
                $statRestime = $resTime / $sum;
                $statPure = $pureNum / $sum * 100;
                $statCombo = $comboNum / $sum * 100;
                $statSupport = $supportNum / $sum * 100;
                $statPure1 = $pureNum1 / $sum * 100;
                $statCombo1 = $comboNum1 / $sum * 100;
                $statSupport1 = $supportNum1 / $sum * 100;
                $statGa = $gaNum / $sum * 100;
                $statEquCh = $equalCh / $notVaries * 100;

                echo "location: ";
                print_r($location . "-" . $city);
                echo PHP_EOL;
                print_r($sum);
                echo PHP_EOL;
                echo "hit: ";
                print_r($statHit);
                echo PHP_EOL;
                echo "error: ";
                print_r($statError);
                echo PHP_EOL;
                echo "200status: ";
                print_r($statPass);
                echo PHP_EOL;
                echo "restime: ";
                print_r($statRestime);
                echo PHP_EOL;
                echo "only: ";
                print_r($onlyHave);
                echo PHP_EOL;
                echo "dont: ";
                print_r($dontHave);
                echo PHP_EOL;
                echo "faster: ";
                print_r($statFaster);
                echo PHP_EOL;

                $date1 = strval(date('Y-m-d H:i:s'));

                $data['date'] = $date1;
                $data['GA'] = $statGa;
                $data['faster'] = $statFaster;
                $data['equalGPlay'] = $statEquCh;
                $data['fasterPure'] = $statPure;
                $data['laterPure'] = $statPure1;
                $data['fasterCombo'] = $statCombo;
                $data['laterCombo'] = $statCombo1;
                $data['fasterSupport'] = $statSupport;
                $data['laterSupport'] = $statSupport1;
                $data['HIT'] = $statHit;
                $data['status200'] = $statPass;
                $data['restimeAvg'] = $statRestime;
                $data['404'] = $statError;
                $data['only'] = $onlyHave;
                $data['none'] = $dontHave;
                $data['totalApp'] = $sum;
                $data['location'] = $location;
                $data['city'] = $city;

                DB::table('stats')->insertOrIgnore($data);
            }
        }
    }

    public function putData()
    {
        $slaves = DB::table('location')
            ->select("ip", 'topic')
            ->where("topic", '!=', '0')
            ->where("status", "0")
            ->get();

        foreach ($slaves as $slave) {
            $ip = $slave->ip;
            $topic = $slave->topic;

            if ($topic == 1) {
                DB::connection()->getPdo()->exec("load data infile '/var/www/html/compareVersion/dataCrawl$topic.txt' ignore into table versioncompare(appid, urlId, isGA, location, city, apktrending, apkpure, apkmonk, apkcombo, apksupport, chplay, dateCheck, statusCode, cacheStatus, cfRay, title, resTime)");
                shell_exec("rm -f /var/www/html/compareVersion/data.txt /var/www/html/compareVersion/dataCrawl$topic.txt");
            } else {
                shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@$ip 'firewall-cmd --permanent --zone=public --add-port='80/tcp' && firewall-cmd --reload'");
                shell_exec("wget -N -P /var/www/html/compareVersion/ http://$ip/compareVersion/dataCrawl$topic.txt");
                DB::connection()->getPdo()->exec("load data infile '/var/www/html/compareVersion/dataCrawl$topic.txt' ignore into table versioncompare(appid, urlId, isGA, location, city, apktrending, apkpure, apkmonk, apkcombo, apksupport, chplay, dateCheck, statusCode, cacheStatus, cfRay, title, resTime)");
                shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@$ip 'rm -f /var/www/html/compareVersion/dataCrawl$topic.txt /var/www/html/compareVersion/data.txt'");
                shell_exec("rm -f /var/www/html/compareVersion/data/data$topic.txt /var/www/html/compareVersion/dataCrawl$topic.txt");
            }
        }
    }

    public function compareData()
    {
        $slaves = DB::table('location')
            ->select("ip", "location", 'city', 'topic')
            ->where("topic", '!=', '0')
            ->where("status", "0")
            ->get();

        foreach ($slaves as $slave) {
            $ip = $slave->ip;
            $location = $slave->location;
            $topic = $slave->topic;
            $city = $slave->city;

            $apps = DB::table('url')
                ->select('id', 'url', 'type')
                ->where('isList', '0')
                ->limit(100)
                ->inRandomOrder()
                ->get();

            foreach ($apps as $app) {
                $dataStr = $app->id . "\t" . $app->url . "\t" . $app->type . "\t" . $location . "\t" . $city . "\t" . $ip . "\n";
                if ($topic == 1) {
                    file_put_contents("/var/www/html/compareVersion/data.txt", $dataStr, FILE_APPEND);
                } else {
                    file_put_contents("/var/www/html/compareVersion/data/data" . $topic . ".txt", $dataStr, FILE_APPEND);
                }
            }

            if ($topic == 1) {
                shell_exec("nohup php /var/www/html/compareVersion/compareVersion.php $topic >/dev/null 2>&1&");
            } else {
                shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'rm -f /var/www/html/compareVersion/simple_html_dom.php /var/www/html/compareVersion/compareVersion.php /var/www/html/compareVersion/commandSlave.php'");
                shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'wget -N -O /var/www/html/compareVersion/simple_html_dom.php http://159.65.130.206/compareVersion/simple_html_dom.txt'");
                shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'wget -N -O /var/www/html/compareVersion/commandSlave.php http://159.65.130.206/compareVersion/commandSlave.txt'");
                shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'wget -N -O /var/www/html/compareVersion/data.txt http://159.65.130.206/compareVersion/data/data" . $topic . ".txt'");
                shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'wget -N -O /var/www/html/compareVersion/compareVersion.php http://159.65.130.206/compareVersion/compareVersion.txt'");
                sleep(5);
                shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'nohup php /var/www/html/compareVersion/compareVersion.php " . $topic . " >/dev/null 2>&1&'");
            }
        }
    }

    public function getdata()
    {
        $slaves = DB::table('location')
            ->select("ip", "location", 'city', 'topic', 'status')
            ->where("topic", '!=', '0')
            ->where("status", '1')
            ->get();
        // print_r($slaves);die;
        foreach ($slaves as $slave) {
            if ($slave->status == 1) {
                $location = $slave->location;
                $topic = $slave->topic;
                $city = $slave->city;
                $ip = $slave->ip;

                $apps = DB::table('url')
                    ->select('id', 'url', 'type')
                    ->where('isList', '0')
                    ->inRandomOrder()
                    ->limit(100)
                    ->get();

                foreach ($apps as $app) {
                    $dataStr = $app->id . "\t" . $app->url . "\t" . $app->type . "\t" . $location . "\t" . $city . "\t" . $ip . "\n";
                    if ($topic == 1) {
                        file_put_contents("/var/www/html/compareVersion/data.txt", $dataStr, FILE_APPEND);
                    } else {
                        file_put_contents("/var/www/html/compareVersion/data/data" . $topic . ".txt", $dataStr, FILE_APPEND);
                    }
                }
                if ($topic == 1) {
                    shell_exec("nohup php /var/www/html/compareVersion/compareVersion.php $topic >/dev/null 2>&1&");
                } else {
                    shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'rm -f /var/www/html/compareVersion/simple_html_dom.php /var/www/html/compareVersion/compareVersion.php /var/www/html/compareVersion/commandSlave.php'");
                    shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'wget -N -O /var/www/html/compareVersion/simple_html_dom.php http://159.65.130.206/compareVersion/simple_html_dom.txt'");
                    shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'wget -N -O /var/www/html/compareVersion/compareVersion.php http://159.65.130.206/compareVersion/compareVersion.txt'");
                    shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'wget -N -O /var/www/html/compareVersion/commandSlave.php http://159.65.130.206/compareVersion/commandSlave.txt'");
                    // shell_exec('ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@' . $ip . ' "ps -ef|awk \'/subcribeSlave.jar ' . $topic . '/{print $2}\'|xargs kill"');
                    // shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'nohup java -jar /var/www/html/compareVersion/subcribeSlave.jar " . $topic . " >/dev/null 2>&1&'");
                    sleep(2);
                    shell_exec("java -jar /var/www/html/compareVersion/publishMaster.jar downloaded:" . $topic . ":" . $ip . " " . $topic);
                }
            }
        }
    }

    public function setupVPS()
    {
        $slaves = DB::table('location')
            ->select("ip", 'topic')
            ->where("topic", '!=', '0')
            ->where("status", "0")
            ->get();

        foreach ($slaves as $slave) {
            $ip = $slave->ip;
            $topic = $slave->topic;
            if ($topic != 1) {
                shell_exec("firewall-cmd --permanent --zone=public --add-rich-rule='rule family='ipv4' source address='" . $ip . "/32' port protocol='tcp' port='80' accept' && firewall-cmd --reload");
                shell_exec("firewall-cmd --permanent --zone=public --add-rich-rule='rule family='ipv4' source address='" . $ip . "/32' port protocol='tcp' port='1883' accept' && firewall-cmd --reload");
                shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'yum -y update && yum -y install java-1.8.0-openjdk httpd wget php && systemctl start httpd.service && systemctl enable httpd.service && systemctl restart httpd.service'");
                shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'mkdir -m 0777 -p /var/www/html/compareVersion'");
                shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'wget -N -P /var/www/html/compareVersion/ http://159.65.130.206/compareVersion/subcribeSlave.jar'");
                shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'wget -N -P /var/www/html/compareVersion/ http://159.65.130.206/compareVersion/publishSlave.jar'");
                DB::table('location')
                ->where('topic', $topic)
                ->update(['status' => '1']);
            }
        }
    }

    public function deleteDataFile($ip, $topic)
    {
        shell_exec("rm -f /var/www/html/compareVersion/data/data" . $topic . ".txt");
        shell_exec("java -jar /var/www/html/compareVersion/publishMaster.jar run:" . $topic . ":" . $ip . " " . $topic);
    }

    public function downUpdate($ip, $topic)
    {
        if ($topic == "1") {
            shell_exec("cp /var/www/html/compareVersion/dataCrawl1.txt /var/lib/mysql/tovi/");
            DB::connection()->getPdo()->exec("load data infile 'dataCrawl1.txt' ignore into table versioncompare(appid, urlId, isGA, location, city, apktrending, variantTrending, apkpure, variantPure, apkmonk, apkcombo, apksupport, chplay, dateCheck, statusCode, cacheStatus, cfRay, title, resTime, canDownApk, sizeOnWebApk, sizeOnFileApk, downSpeedApk, canDownObb, sizeOnWebObb, sizeOnFileObb, downSpeedObb, canDownBundle, sizeOnWebBundle, sizeOnFileBundle, downSpeedBundle)");
            shell_exec("rm -f /var/lib/mysql/tovi/dataCrawl1.txt /var/www/html/compareVersion/data.txt /var/www/html/compareVersion/dataCrawl1.txt");
        } else {
            shell_exec("wget -N -P /var/lib/mysql/tovi/ http://" . $ip . "/compareVersion/dataCrawl" . $topic . ".txt");
            sleep(3);
            DB::connection()->getPdo()->exec("load data infile 'dataCrawl" . $topic . ".txt' ignore into table versioncompare(appid, urlId, isGA, location, city, apktrending, variantTrending, apkpure, variantPure, apkmonk, apkcombo, apksupport, chplay, dateCheck, statusCode, cacheStatus, cfRay, title, resTime, canDownApk, sizeOnWebApk, sizeOnFileApk, downSpeedApk, canDownObb, sizeOnWebObb, sizeOnFileObb, downSpeedObb, canDownBundle, sizeOnWebBundle, sizeOnFileBundle, downSpeedBundle)");
            shell_exec("ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@" . $ip . " 'rm -f /var/www/html/compareVersion/data.txt /var/www/html/compareVersion/dataCrawl" . $topic . ".txt'");
            shell_exec("rm -f /var/lib/mysql/tovi/dataCrawl" . $topic . ".txt");
        }
        shell_exec("php /var/www/html/compareVersion/telegramNoti.php completed:" . $ip . ":" . $topic);
    }

    public function noteChange($message)
    {
        shell_exec("php /var/www/html/compareVersion/telegramNoti.php " . $message);
    }

    public function loaddata()
    {
        $results = DB::table('url')
            ->select("id", "url")
            ->where('type', 'GA')
            ->get();

        foreach ($results as $result) {
            $id = $result->id;
            $url = $result->url;
            $tmpUrl1 = explode("/", $url);
            if (sizeof($tmpUrl1) == 5) {
                if ($tmpUrl1[3] == "dev" || $tmpUrl1[3] == "category") {
                    DB::table("url")
                        ->where("id", $id)
                        ->update(["isList" => "1"]);
                }
            } else {
                DB::table("url")
                    ->where("id", $id)
                    ->update(["isList" => "1"]);
            }
        }
    }

    public function crawl()
    {
        $result = DB::table('url')->select("id", "url")
            ->where("status", "0")
            ->limit(10)
            ->get();

        while ($result != NULL) {
            foreach ($result as $app) {
                $urlLink = $app->url;
                $id = $app->id;
                echo $urlLink . PHP_EOL;
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52',
                    CURLOPT_URL => $urlLink,
                    CURLOPT_FOLLOWLOCATION => true
                ));
                curl_setopt($curl, CURLOPT_HEADER, true);
                $html = curl_exec($curl);
                curl_close($curl);
                $doc = str_get_html($html);
                if ($doc != NULL) {

                    foreach ($doc->find("a") as $element) {
                        $link = $element->href;
                        $linkPath = parse_url($link, PHP_URL_PATH);
                        $linkScheme = parse_url($link, PHP_URL_SCHEME);
                        $linkHost = parse_url($link, PHP_URL_HOST);
                        if ($linkHost == NULL && $linkScheme == NULL) {
                            $linkHost = "apktrending.com";
                            $linkScheme = "https";
                        }

                        if (substr_count($linkPath, "/") > 0 && substr_count($linkPath, "/") < 3 && strcmp($linkHost, "apktrending.com") == 0) {
                            if (strpos($linkPath, "dev") != FALSE || substr_count($linkPath, "/") < 2 || strpos($linkPath, ".") == FALSE || strpos($linkPath, "category") != FALSE) {
                                $data['url'] = $linkScheme . "://" . $linkHost . $linkPath;
                                $data['isList'] = 1;
                                $data['status'] = 0;
                                $data['pageCrawl'] = $id;
                                DB::table('url')->insertOrIgnore($data);
                                echo "Inserted isList links into file" . PHP_EOL;
                            } else {
                                $data['url'] = $linkScheme . "://" . $linkHost . $linkPath;
                                $data['isList'] = 0;
                                $data['status'] = 0;
                                $data['pageCrawl'] = $id;
                                DB::table('url')->insertOrIgnore($data);
                                echo "Inserted app links into file" . PHP_EOL;
                            }
                        }
                    }
                    DB::table('url')->where("id", $id)->update([
                        "status" => '1'
                    ]);
                } else {
                    echo "Khong tim thay trang!" . PHP_EOL;
                }
            }
            $result = DB::table('url')->select("id", "url")
                ->where("status", "0")
                ->limit(10)
                ->get();
        }
    }

    public function compVN()
    {
        $location = "VN";
        $city = "";
        $apps = DB::table('url')
            ->select('id', 'url', 'type')
            ->where('isList', '0')
            ->limit(100)
            ->inRandomOrder()
            ->get();

        foreach ($apps as $app) {
            $dataStr = $app->id . "\t" . $app->url . "\t" . $app->type . "\t" . $location . "\t" . $city . "\n";
            file_put_contents("data/data.txt", $dataStr, FILE_APPEND);
        }

        shell_exec("php D:/DDat/Tovi/Tovi_tool/PHP/crawlURL/compareVersionVN.php");

        DB::connection()->getPdo()->exec("load data local infile 'D:/Programs/xampp/htdocs/laravel/data/dataCrawl.txt' ignore into table versioncompare(appid, urlId, isGA, location, city, apktrending, apkpure, apkmonk, apkcombo, apksupport, chplay, dateCheck, statusCode, cacheStatus, cfRay, title, resTime)");

        shell_exec("del /f D:/Programs/xampp/htdocs/laravel/data/dataCrawl.txt");
        shell_exec("del /f D:/Programs/xampp/htdocs/laravel/data/data.txt");
    }

    public function kill()
    {
        $slaves = DB::table('location')
            ->select("ip", "location", 'city', 'topic', 'status')
            ->where("topic", '!=', '0')
            // ->where("status", '0')
            ->get();

        foreach ($slaves as $slave) {
            $topic = $slave->topic;
            $ip = $slave->ip;
            if ($slave->status != 1) {
                shell_exec('ssh -o StrictHostKeyChecking=no -i ~/private_key1 root@' . $ip . ' "ps -ef|awk \'/compareVersion.php ' . $topic . '/{print $2}\'|xargs kill"');
            } else {
                shell_exec('ps -ef|awk \'/compareVersion.php ' . $topic . '/{print $2}\'|xargs kill');
            }
        }
    }

    // public function testLoadData(){
    //     $query = "load data infile 'dataTest.txt' ignore into table versioncompare(appid, urlId, isGA, location, city, apktrending, apkpure, apkmonk, apkcombo, apksupport, chplay, dateCheck, statusCode, cacheStatus, cfRay, title, resTime)";
    //     DB::connection()->getPdo()->exec($query);
    // }

}