<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Faker;
use Hash;
use DB;
use App\Http\Controllers\CouponController;

class UpdateCouponExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coupon:update {type=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Expired Coupon = 1, Update Url Coupon = 2';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(CouponController $coupon)
    {
        $type = $this->argument('type');
		if ($type == '1') {
			$coupon->UpdateExpiredCoupon();
			$this->info('Update Expired Coupon Success');
		} else {
			$list_page = $coupon->NewStoreCoupon();
			$coupon->AddCoupon($list_page);
			$this->info('Add Coupon New Store Success');
		}
    }
}
