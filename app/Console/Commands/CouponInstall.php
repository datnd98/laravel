<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Faker;
use Hash;
use DB;
use App\Http\Controllers\CouponController;

class CouponInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'coupon:insert {start=0} {end=50}';
    protected $signature = 'coupon:insert {url=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run coupon insert, update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(CouponController $coupon)
    {
        $url = $this->argument('url');
		$list_page = $coupon->AddLink($url);
		$coupon->AddCoupon($list_page);
		$this->info('Coupon insert success');
		
		//// ReInsert Search List
		
        // $start = $this->argument('start');
        // $end = $this->argument('end');
		// $coupon->ReInsertUrlSearch($start, $end);
		// $this->info('ReInsert Search List success '.$end);
    }
}
