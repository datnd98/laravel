<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Faker;
use Hash;
use DB;
use App\Http\Controllers\CouponController;

class CouponSearch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coupon:search {type=1} {start=1} {end=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '1= update search detail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(CouponController $coupon)
    {
        $type = $this->argument('type');
		if ($type == '1') {
			$coupon->SearchDetail();
			$this->info('Update Expired Coupon Success');
		} elseif ($type == '2') {
			$start = $this->argument('start');
			$end = $this->argument('end');
			$list_page = $coupon->GetMoreKeySearch($start,$end);
			$this->info('Add Key Search Success ' .$end);
		} elseif ($type == '3') {
			$start = $this->argument('start');
			$end = $this->argument('end');
			$list_page = $coupon->GetDomain($start,$end);
			$this->info('Add Domain Success '.$end);
		}
    }
}
