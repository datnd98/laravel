<?php

namespace App\Console\Commands;

use App\Http\Controllers\crawlCH as ControllersCrawlCH;
use Illuminate\Console\Command;

class crawlCH extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawlCH {state=1} {variable1=1} {variable2=1} {variable3=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description: num % thread == 0, variable1 = {appid, num}, variable2 = {id_lang, limit}, variable3 = {id_country, offset}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ControllersCrawlCH $crawler)
    {
        $state = $this->argument('state');
        switch ($state) {
            case '1':
                $num = $this->argument('variable1');
                $limit = $this->argument('variable2');
                $offset = $this->argument('variable3');
                $crawler->RunautoUpdateDevID($num, $limit, $offset);
                break;

            case '2':
                $offset = $this->argument('variable1');
                $limit = $this->argument('variable2');
                $crawler->runCrawl($offset, $limit);
                break;

            case '3':
                $appid = $this->argument('variable1');
                $id_lang = $this->argument('variable2');
                $id_country = $this->argument('variable3');
                $crawler->GetAppInfo($appid, $id_lang, $id_country);
                break;
            default:
                # code...
                break;
        }
    }
}
