<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Faker;
use Hash;
use DB;
use App\Http\Controllers\CouponController;

class InsertAlexa extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coupon:alexa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert Alexa Rank';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(CouponController $coupon)
    {
        $coupon->UpdateAlexa();
		$this->info('Update Alexa Rank Success');
    }
}
