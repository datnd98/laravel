<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\versionCompareDistributed;

class verComp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verComp {message=1} {topic=1} {ip=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(versionCompareDistributed $comp)
    {
        $topic = $this->argument("topic");
        $ip = $this->argument("ip");
        $message = $this->argument("message");

        switch ($message) {
            case 'downloaded':
                $comp->deleteDataFile($ip, $topic);
                break;

            case '1':
                $comp->loaddata();
                break;

            case "finished":
                $comp->downUpdate($ip, $topic);
                break;

            case "start":
                $comp->getdata();
                break;

            case "compareData":
                $comp->compareData();
                break;

            case "put":
                $comp->putData();
                break;

            case "crawl":
                $comp->crawl();
                break;

            case "stats":
                $comp->stats();
                break;

            case "VN":
                $comp->compVN();
                break;

            case "restart":
                $comp->setupVPS();
                break;

            case "killall":
                $comp->kill();
                break;
                // case "testLoad":
                //     $comp->testLoadData();
                //     break;
        }
        if (strpos($message, "-") != FALSE) {
            $comp->noteChange($message);
        }
    }
}
